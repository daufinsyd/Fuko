package com.example.z003sdxe.myapplication;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import static com.example.z003sdxe.myapplication.MainActivity.fileList;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayFileListActivity extends AppCompatActivity {
    private static final int READ_REQUEST_CODE = 42;
    private ListView fileListView;
    private ArrayAdapter arrayAdapter;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_file_list);

        // TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        fileListView = (ListView) findViewById(R.id.fileListView);
        fileListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);  // enables check state
        arrayAdapter = new ArrayAdapter(this, R.layout.simple_list_item_1, fileList);  // Linked to fileList
        fileListView.setAdapter(arrayAdapter);
        floatingActionButton = ((FloatingActionButton) findViewById(R.id.floatingActionButton));

        addListenerFloatingActionButton();
        addListenerListView();
    }

    @Override
    public void onResume(){
        super.onResume();
        updateRemoveSelectionButton();
    }

    public void removeAllFiles(View view){
        fileList.clear();
        arrayAdapter.notifyDataSetChanged();
    }

    public void removeSelection(){
        if (arrayAdapter.getCount() > 0 && fileListView.getCheckedItemCount() > 0) {
            SparseBooleanArray itemsStatus = fileListView.getCheckedItemPositions();
            ((TextView) findViewById(R.id.textView2)).setText(itemsStatus.toString());
            int j = 0;  // If many itemes (see below)
            for (Integer i = 0; i < itemsStatus.size(); i++) {
                Integer key = itemsStatus.keyAt(i);
                Boolean value = itemsStatus.valueAt(i);
                if (value) {
                    fileListView.setItemChecked(key, false);  // force listView to uncheck item before deleting it; otherwise the item is deleted but still counted as checkd

                    fileList.remove((int) (key - j));  // Integer > object WHEREAS int isn't considered as an Object
                    // So if remove(Integer) > search for the Object in the list
                    // whereas remove(int) > asearch for the position
                    arrayAdapter.notifyDataSetChanged();
                    ((TextView) findViewById(R.id.textView2)).setText(String.format(Locale.FRANCE, "%s", fileList.toString()));

                    j++;  // If a elemt is deleted > then the other are slep to the left ; but itemsStatus
                    // isn't changed accordingly > so we need to decrement "key" >> we use j
                }
            }
        }
        updateRemoveSelectionButton();
    }

    /**
     * Start select file(s) activity
     * @param view
     */
    public void addFile(View view){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);  // juste to read/import the file
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);  // allow selection of multiples files
        String [] mimeTypes = {"audio/*", "video/*"};  // TODO Ogg doesn't work
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.setType("*/*");

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if(data != null){
                String fileName = null;  // name display for the file
                MyFileItem myFileItem = null;  // item
                TextView textView = (TextView) findViewById(R.id.textView2);
                ClipData files = data.getClipData();
                if(files != null) {
                    // If many files selected
                    Integer fileCount = files.getItemCount();
                    textView.setText(String.format(Locale.FRANCE, "%d", fileCount));
                    for (Integer i = 0; i < fileCount; i++){
                        textView.setText(String.format(Locale.FRANCE, "%d", i));
                        Uri uri = files.getItemAt(i).getUri();
                        String filePath = FileUtils.getRealPathFromURI_API19(getBaseContext(), uri);  // Path
                        myFileItem = new MyFileItem(uri, filePath);
                        fileList.add(myFileItem);
                    }
                    Toast.makeText(this, String.format(Locale.FRANCE, "%d new files added", fileCount), Toast.LENGTH_SHORT).show();
                }
                else {
                    // Only one file selected
                    Uri uri = data.getData();
                    String filePath = FileUtils.getRealPathFromURI_API19(getBaseContext(), uri);  // Path
                    myFileItem = new MyFileItem(uri, filePath);
                    fileList.add(myFileItem);
                    Toast.makeText(this, "New file added", Toast.LENGTH_SHORT).show();
                }
                arrayAdapter.notifyDataSetChanged();  // Updates UI
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                // if not > then restore activity doesn't work; don't know why
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Set visibility of the "remove selection" button
     */
    private void updateRemoveSelectionButton(){
        if (fileListView.getCheckedItemCount() > 0)
            floatingActionButton.setVisibility(View.VISIBLE);
        else floatingActionButton.setVisibility(View.INVISIBLE);
    }

    public void addListenerFloatingActionButton(){
        floatingActionButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                removeSelection();
            }
        });
    }

    public void addListenerListView(){
        fileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateRemoveSelectionButton();
            }
        });
    }

}
