package com.example.z003sdxe.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

class Enums {
    enum AudioCodec {
        COPY("copy", 0, "", "", -1, 0, 0, 0),
        OGG("ogg", 1, "libvorbis", ".ogg", 0, 0, 10, 4),
        FLAC("flac", 2, "flac", ".flac", 1, 0, 12, 5),
        MP3("mp3", 3, "libmp3lame", ".mp3", -1, 0, 0, 0),
        AAC("aac (native)", 4, "aac", ".aac", -1, 0, 0, 0); // todo activate VBR for aac

        private String codec;
        private String ffpmegCode;
        private String extension;
        private int value;  // use for position in spinner

        private int qualityType;  // 0: quality, 1: compress level
        private int qualityMin;
        private int qualityMax;
        private int qualityDefault;


        AudioCodec(String codec, int value, String ffpmegCode, String extension, int qType, int qMin, int qMax, int qDef) {
            this.codec = codec;
            this.value = value;
            this.ffpmegCode = ffpmegCode;
            this.extension = extension;
            qualityType = qType;
            qualityMin = qMin;
            qualityMax = qMax;
            qualityDefault = qDef;
        }

        @Override
        public String toString() {
            return codec;
        }

        public int getValue(){
            return value;
        }

        public String getDotExtension() {return extension;}

        public String getFfpmegCode() {return ffpmegCode;}

        public int getQualityType() {
            return qualityType;
        }

        public int getQualityMin() {
            return qualityMin;
        }

        public int getQualityMax() {
            return qualityMax;
        }

        public int getQualityDefault() {
            return qualityDefault;
        }

    }

    enum VideoCodec {
        COPY("copy", 0, "copy", "", -1, 0, 0, 0),
        OGG("ogv", 1, "libtheora", ".ogv", -1, 0, 0, 0),
        MP4("h264", 2, "libx264", ".mkv", -1, 0, 0, 0);

        private String codec;
        private String ffpmegCode;
        private String extension;
        private int value;  // use for position in spinner

        private int qualityType;  // 0: quality
        private int qualityMin;
        private int qualityMax;
        private int qualityDefault;

        VideoCodec(String codec, int value, String ffpmegCode, String extension, int qType, int qMin, int qMax, int qDef) {
            this.codec = codec;
            this.value = value;
            this.ffpmegCode = ffpmegCode;
            this.extension = extension;
            qualityType = qType;
            qualityMin = qMin;
            qualityMax = qMax;
            qualityDefault = qDef;
        }

        @Override
        public String toString() {
            return codec;
        }

        public int getValue(){
            return value;
        }

        public String getDotExtension() {return extension;}

        public String getFfpmegCode() {return ffpmegCode;}

        public int getQualityType() {
            return qualityType;
        }

        public int getQualityMin() {
            return qualityMin;
        }

        public int getQualityMax() {
            return qualityMax;
        }

        public int getQualityDefault() {
            return qualityDefault;
        }
    }
}