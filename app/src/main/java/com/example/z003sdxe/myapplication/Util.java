package com.example.z003sdxe.myapplication;

/**
 *
 * https://raw.githubusercontent.com/adrielcafe/AndroidAudioConverter/master/app/src/main/java/cafe/adriel/androidaudioconverter/sample/Util.java
 */

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

public class Util {

    /**
     * Check permissions
     * @param activity
     * @param permission requested permission to check
     */
    public static void requestPermission(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, 0);
        }
    }

    /**
     * En/Disable rangeSeekBar
     * @param context context
     * @param uri uri of the disired file
     * @return length in milliseconds of the file
     */
    static int getFileDurationTime(Context context, Uri uri){
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(FileUtils.getRealPathFromURI_API19(context, uri));
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return Integer.parseInt(durationStr);
    }
}