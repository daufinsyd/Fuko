package com.example.z003sdxe.myapplication;

import android.net.Uri;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

class MyFileItem {
    private Uri uri;
    private String fileName;
    private String completePath;

    MyFileItem(Uri uri, String completePath) {
        super();
        this.uri = uri;
        this.completePath = completePath;
        this.fileName = completePath.split("/")[(completePath.split("/")).length - 1];
    }

    @Override
    public String toString() {return completePath;}

    public String getFileName() {return fileName;}

    public Uri getUri() {return uri;}

}
