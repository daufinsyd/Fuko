package com.example.z003sdxe.myapplication;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import javax.xml.datatype.Duration;

import io.apptik.widget.MultiSlider;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayOptions extends AppCompatActivity {
    CheckBox checkBoxCustomTime;
    MultiSlider multiSliderCustomTimme;
    TextView textViewCustomTimeStart;
    TextView textViewCustomTimeEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_options);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        checkBoxCustomTime = ((CheckBox)findViewById(R.id.checkBoxCustomTime));
        multiSliderCustomTimme = ((MultiSlider)findViewById(R.id.range_sliderCustomTime));
        textViewCustomTimeStart = ((TextView)findViewById(R.id.textViewCurstomTimeStart));
        textViewCustomTimeEnd = ((TextView)findViewById(R.id.textViewCustomTimeEnd));

        addCheckBoxListener();
        addMultiSliderListerner();

        // todo here ? (> onResume or even in Main or file ?)
        if (MainActivity.fileList.size() == 1)
            MainActivity.durationTime = Util.getFileDurationTime(this, MainActivity.fileList.get(0).getUri());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                // if not > then restore activity doesn't work; don't know why
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        ((CheckBox)findViewById(R.id.checkBoxDeleteOrig)).setChecked(MainActivity.deleteOrig);
        ((CheckBox)findViewById(R.id.checkBoxSubtitles)).setChecked(MainActivity.keepSubtitles);

        if (MainActivity.fileList.size() != 1) {
            noOneFile_customTime(this, false);  // if there is no file, no need to advert the user
        }
        else {
            checkBoxCustomTime.setChecked(MainActivity.customTime);
            multiSliderCustomTimme.setEnabled(MainActivity.customTime);
            // alpha = isEnabled + !isEnabled * 0.5
            Float alpha = (float)((MainActivity.customTime) ? 1 : 0) + (float)(((MainActivity.customTime) ? 0 : 1) * 0.5);
            multiSliderCustomTimme.setAlpha(alpha);
            multiSliderCustomTimme.setMin(0);  // todo remove ?
            multiSliderCustomTimme.setMax(MainActivity.durationTime);
            Toast.makeText(this, String.format("%d",MainActivity.customStartTime.getDuration()), Toast.LENGTH_SHORT).show();

            // WARNING ! First set second thumb THEN set the first (because thumb0 < thumb1)
            multiSliderCustomTimme.getThumb(1).setValue(MainActivity.customEndTime.getDuration());
            multiSliderCustomTimme.getThumb(0).setValue(MainActivity.customStartTime.getDuration());
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        MainActivity.deleteOrig = ((CheckBox)findViewById(R.id.checkBoxDeleteOrig)).isChecked();
        MainActivity.keepSubtitles = ((CheckBox)findViewById(R.id.checkBoxSubtitles)).isChecked();

        MainActivity.customTime = checkBoxCustomTime.isChecked();
        MainActivity.customStartTime.setDuration(multiSliderCustomTimme.getThumb(0).getValue());
        MainActivity.customEndTime.setDuration(multiSliderCustomTimme.getThumb(1).getValue());
    }

    void noOneFile_customTime(Context con, Boolean showMessage){
        if (showMessage)
            Toast.makeText(con, con.getString(R.string.customTime_error1), Toast.LENGTH_SHORT).show();
        checkBoxCustomTime.setChecked(false);
        multiSliderCustomTimme.setEnabled(false);
        multiSliderCustomTimme.setAlpha((float)0.5);
    }

    /**
     * En/Disable rangeSeekBar
     * @param
     * @return
     */
    public void addCheckBoxListener(){
        checkBoxCustomTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Can only edit time for one file
                if (MainActivity.fileList.size() != 1) {
                    noOneFile_customTime(v.getContext(), true);
                }
                else {
                    multiSliderCustomTimme.setEnabled(checkBoxCustomTime.isChecked());
                    // (-value + 1,5) >> 0.5 > 1 > 0.5 > 1 ...
                    multiSliderCustomTimme.setAlpha((float) (-multiSliderCustomTimme.getAlpha() + 1.5));
                }
            }
        });
    }

    public void addMultiSliderListerner(){
        multiSliderCustomTimme.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex == 0) {
                    MainActivity.customStartTime.setDuration(value);
                    textViewCustomTimeStart.setText(MainActivity.customStartTime.getHMSprecise());
                }else{
                    MainActivity.customEndTime.setDuration(value);
                    textViewCustomTimeEnd.setText(MainActivity.customEndTime.getHMSprecise());
                }
            }
        });
    }
}
