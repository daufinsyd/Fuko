package com.example.z003sdxe.myapplication;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.widget.Toast;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class FileUtils {
    /**
     * @param context
     * @param uri uri to the requested file
     * @return real path of the file
     */
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        // considers that the provided uri links to a file
        return getRealPathFromURI_API19(context, uri, false);
    }

    /**
     * @param context
     * @param uri uri to the requested file or folder
     * @param isFolder set true if the uri corresponds to a folder
     * @return real path of the file or the folder
     */
    public static String getRealPathFromURI_API19(Context context, Uri uri, Boolean isFolder) {
        //https://stackoverflow.com/questions/42110882/get-real-path-from-uri-of-file-in-sdcard-marshmallow
        String filePath = "";
        // ExternalStorageProvider
        if (isFolder || isExternalStorageDocument(uri)) {
            String docId = null;
            // Folder
            if(isFolder){
                docId = DocumentsContract.getTreeDocumentId(uri);
            }
            // File
            else {
                docId = DocumentsContract.getDocumentId(uri);
            }
            final String[] split = docId.split(":");  // eg. /storage/sdCard:path/to/file
            final String type = split[0];
            Toast.makeText(context, "TYPE " + type, Toast.LENGTH_SHORT).show();  //todo remove
            if ("primary".equalsIgnoreCase(type) && !isFolder) {
                filePath = Environment.getExternalStorageDirectory() + "/";
            } else if ("home".equalsIgnoreCase(type)){  // todo if not folder ?
                // todo Seems to work for now. But is it correct ?
                filePath = Environment.getExternalStorageDirectory() + "/Documents/";
            }
            else {
                filePath = "/storage/" + type + "/";
            }
            if(split.length > 1){
                // If it's folder and it's at the root (eg. /storage/sdCard:) then split[1] doesn't exist
                filePath = filePath + split[1];
            }
            return filePath;

        } else if (isDownloadsDocument(uri)) {
            // DownloadsProvider

            final String id = DocumentsContract.getDocumentId(uri);
            // the two following lines were commented out and it didn't use contentUri (use uri)
            final Uri contentUri = ContentUris.withAppendedId(
            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

            Cursor cursor = null;
            final String column = "_data";
            final String[] projection = {column};

            try {
                // it gave uri instead of contentUri
                cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    String result = cursor.getString(index);
                    cursor.close();
                    return result;
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }

        } else if (DocumentsContract.isDocumentUri(context, uri)) {
            // MediaProvider
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String[] ids = wholeID.split(":");
            String id;
            String type;
            if (ids.length > 1) {
                id = ids[1];
                type = ids[0];
            } else {
                id = ids[0];
                type = ids[0];
            }

            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }

            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{id};
            final String column = "_data";
            final String[] projection = {column};
            Cursor cursor = context.getContentResolver().query(contentUri,
                    projection, selection, selectionArgs, null);

            if (cursor != null) {
                int columnIndex = cursor.getColumnIndex(column);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            return filePath;
        } else {
            String[] proj = {MediaStore.Audio.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                if (cursor.moveToFirst())
                    filePath = cursor.getString(column_index);
                cursor.close();
            }


            return filePath;
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

}
