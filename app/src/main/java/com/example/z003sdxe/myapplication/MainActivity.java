package com.example.z003sdxe.myapplication;

import android.app.Activity;
import android.app.Application;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.BoolRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;

import android.Manifest;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;
import static android.support.v4.content.PermissionChecker.PERMISSION_DENIED;
import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class MainActivity extends AppCompatActivity {
    public static Enums.AudioCodec audioCodec = Enums.AudioCodec.COPY;
    public static Enums.VideoCodec videoCodec = Enums.VideoCodec.COPY;
    public static List<MyFileItem> fileList = new ArrayList<MyFileItem>();
    public static Integer sizeFileList = 0;  // because fileList emptied while converting
    public static Boolean audio = true;
    public static Boolean video = false;
    public static Integer audioQuality = 0;
    public static Integer videoQuality = 0;

    public static Boolean audioSetBirate = false;  // should the bitrate be fixed
    public static Integer audioBitrate = 128;
    public static Boolean videoSetBirate = false;  // should the bitrate be fixed
    public static Integer videoBitrate = 128;

    public static int progressBarVisibility = View.INVISIBLE;
    public static String CONVERT_START_MESSAGE = "CONVERT_START";
    public static String CONVERT_END_MESSAGE = "CONVERT_END";
    public static String CONVERTED_MESSAGE = "B";

    public static Boolean isConverting = false;  // TODO use it for resume, ...
    public static Boolean deleteOrig = false;
    public static Boolean keepSubtitles = false;
    public static Boolean customTime = false;

    public static TimeDuration customStartTime = new TimeDuration(0);
    public static TimeDuration customEndTime = new TimeDuration(1000);
    public static int durationTime = 0;

    // Widgets
    Button buttonOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Util.requestPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        Util.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        findViewById(R.id.progressBar).setVisibility(progressBarVisibility);  // todo onResume
        // Set widgets
        buttonOptions = ((Button)findViewById(R.id.buttonOptions));

        if(savedInstanceState != null){
            audio = savedInstanceState.getBoolean("audio");
            video = savedInstanceState.getBoolean("video");
            audioQuality = savedInstanceState.getInt("audioQuality");
            videoQuality = savedInstanceState.getInt("videoQuality");
            audioCodec = (Enums.AudioCodec)savedInstanceState.getSerializable("audioCodec");
            isConverting = savedInstanceState.getBoolean("isConverting");
            customTime = savedInstanceState.getBoolean("customTime");

            durationTime = savedInstanceState.getInt("durationTime");
            customStartTime = (TimeDuration) savedInstanceState.getSerializable("customStartTime");
            customEndTime = (TimeDuration) savedInstanceState.getSerializable("customEndTime");

            ((Switch) findViewById(R.id.switchAudio)).setChecked(audio);
            findViewById(R.id.buttonConfAudio).setEnabled(audio);
            findViewById(R.id.buttonConfAudio).setClickable(audio);
            ((Switch) findViewById(R.id.switchVideo)).setChecked(video);
            findViewById(R.id.buttonConfVideo).setEnabled(video);
            findViewById(R.id.buttonConfVideo).setClickable(video);

            ((EditText) findViewById(R.id.editTextOutputPath)).setText(savedInstanceState.getCharSequence("outputPath"));
        }

        addListenerOnAudioChg();
        addListenerOnVideoChg();

        // // TODO: 30.08.2017
        ((Button) findViewById(R.id.buttonOutputPath)).setEnabled(false);
        ((EditText) findViewById(R.id.editTextOutputPath)).setEnabled(false);


        // Communication between activities
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(CONVERT_START_MESSAGE) || action.equals(CONVERT_END_MESSAGE)) {
                    if (progressBarVisibility == View.VISIBLE)
                        progressBarVisibility = View.INVISIBLE;
                    else progressBarVisibility = View.VISIBLE;
                    findViewById(R.id.progressBar).setVisibility(progressBarVisibility);
                    findViewById(R.id.buttonConvert).setEnabled(false);
                    findViewById(R.id.buttonFileList).setEnabled(false);
                    findViewById(R.id.buttonConfAudio).setEnabled(false);
                    findViewById(R.id.buttonConfVideo).setEnabled(false);
                    findViewById(R.id.switchAudio).setEnabled(false);
                    findViewById(R.id.switchVideo).setEnabled(false);
                    buttonOptions.setEnabled(false);

                    isConverting = !isConverting;
                }
                else if (action.equals(CONVERTED_MESSAGE)){
                    if (sizeFileList == 0) sizeFileList = 1;  // prevents / by 10
                    ((ProgressBar) findViewById(R.id.progressBarFiles)).setProgress(
                            ((ProgressBar) findViewById(R.id.progressBarFiles)).getProgress() + 100 / (sizeFileList)
                    );

                    // Because ffmpeg is async > disable here
                    if (((ProgressBar) findViewById(R.id.progressBarFiles)).getProgress() >= 99) { // || (sizeFileList > 99 && ((ProgressBar) findViewById(R.id.progressBarFiles)).getProgress() == 100) ) {
                        // cause if 3 files > 0.33 * 3 == 0.99 != 1
                        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
                        findViewById(R.id.buttonConvert).setEnabled(true);
                        findViewById(R.id.buttonFileList).setEnabled(true);
                        findViewById(R.id.buttonConfAudio).setEnabled(true);
                        findViewById(R.id.buttonConfVideo).setEnabled(true);
                        findViewById(R.id.switchAudio).setEnabled(true);
                        findViewById(R.id.switchVideo).setEnabled(true);
                        buttonOptions.setEnabled(true);
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(CONVERT_START_MESSAGE);
        filter.addAction(CONVERT_END_MESSAGE);
        filter.addAction(CONVERTED_MESSAGE);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);

        checkConvertabe();
    }

    @Override
    public void onResume(){
        super.onResume();
        checkConvertabe();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DialogFragment newFragment = null;
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_showFfmpeg:
                Toast.makeText(this, "Ffmpeg selected", Toast.LENGTH_SHORT).show();

                newFragment = DisplayFfmpegVersion.newInstance();
                newFragment.show(fragmentManager, "d");  // if placed after switch > value reuinit ?
                break;
            case R.id.action_about:
                newFragment = DisplayAppInfo.newInstance();
                newFragment.show(fragmentManager, "d");
                break;
            default:
                break;
        }

        return true;
    }

    // SaveInstance
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("audio", audio);
        savedInstanceState.putBoolean("video", video);
        savedInstanceState.putCharSequence("outputPath", (((TextView) findViewById(R.id.editTextOutputPath)).getText()));
        savedInstanceState.putInt("audioQuality", audioQuality);
        savedInstanceState.putInt("videoQuality", videoQuality);
        savedInstanceState.putSerializable("audioCodec", audioCodec);
        savedInstanceState.putSerializable("customStartTime", (Serializable) customStartTime);
        savedInstanceState.putSerializable("customEndTime", (Serializable) customEndTime);
        savedInstanceState.putInt("durationTime", durationTime);
        savedInstanceState.putBoolean("customTime", customTime);
    }

    /**
     * @param view The view.
     * @return Start conf audio activity.
     */
    public void confAudio(View view){
        Intent intent = new Intent(this, DisplayConfAudioActivity.class);
        Switch switch_ = (Switch) findViewById(R.id.switchAudio);
        Boolean audioActivated = switch_.isChecked();
        intent.putExtra("com.example.z003sdxe.myapplication.AUDIO_BOOLEAN", audioActivated);
        startActivity(intent);
    }

    /**
     * @param view The view.
     * @return Start conf video activity.
     */
    public void confVideo(View view){
        Intent intent = new Intent(this, DisplayConfVideoActivity.class);
        startActivity(intent);
    }

    /**
     * @param view The view.
     * @return Start edit options activity.
     */
    public void confOptions(View view){
        Intent intent = new Intent(this, DisplayOptions.class);
        startActivity(intent);
    }

    /**
     * @param view The view.
     * @return Start manage files to convert activity.
     */
    public void fileList(View view) {
        Intent intent = new Intent(this, DisplayFileListActivity.class);
        intent.addFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    /**
     * NON USED
     * @param view The view.
     * @return Start choose output folder activity.
     */
    public void chooseOutputDirectory(View view){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(Intent.createChooser(intent, "Choose dir"), 9999);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9999 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();

                String filePath = FileUtils.getRealPathFromURI_API19(getBaseContext(), uri, true);
                Toast.makeText(this, filePath, Toast.LENGTH_SHORT).show();

                ((TextView) findViewById(R.id.editTextOutputPath)).setText(filePath);
            }
        }
    }


    private static final int WRITE_REQUEST_CODE = 43;
    /**
     * @param view The view.
     * @return Start conversion of the selected files if possible.
     */
    public void convert(View view){
        try {
            // Check perms
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PERMISSION_DENIED)
                Toast.makeText(this, getString(R.string.writePermissionDenied), Toast.LENGTH_SHORT);
            else if (audio || video) {
                ((ProgressBar) findViewById(R.id.progressBarFiles)).setProgress(0);


                //File path = new File( (((TextView) findViewById(R.id.editTextOutputPath)).getText()).toString());

                // InternalStorage
                File dir;
                if (video) {
                     dir = new File(Environment.getExternalStorageDirectory(),
                             Environment.DIRECTORY_MOVIES + "/Fuko");
                }
                else {
                    dir = new File(Environment.getExternalStorageDirectory(),
                            Environment.DIRECTORY_MUSIC + "/Fuko");
                }
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Convert.convert(this, dir, deleteOrig, keepSubtitles);
            }
            else Toast.makeText(this, "Veuillez activer l'audio et/ou la vidéo", Toast.LENGTH_SHORT).show();
        }
        catch (Error e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }


    }

    // AUDIO
    public void addListenerOnAudioChg(){
        Switch audioSw = (Switch) findViewById(R.id.switchAudio);
        audioSw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Switch audioSwitch = (Switch) view;  // Because new context
                Button audioButton = (Button) findViewById(R.id.buttonConfAudio);
                audio = audioSwitch.isChecked();
                if ((audioSwitch).isChecked()) {
                    audioButton.setEnabled(true);
                    audioButton.setClickable(true);
                }
                else {
                    audioButton.setEnabled(false);
                    audioButton.setClickable(false);
                }
                checkConvertabe();
            }
        });
    }

    // VIDEO
    public void addListenerOnVideoChg(){
        final Switch videoSw = (Switch) findViewById(R.id.switchVideo);
        videoSw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Switch videoSwitch = (Switch) view;
                Button videoButton = (Button) findViewById(R.id.buttonConfVideo);
                video = videoSwitch.isChecked();
                if (((Switch) view).isChecked()) {
                    videoButton.setEnabled(true);
                    videoButton.setClickable(true);
                }
                else {
                    videoButton.setEnabled(false);
                    videoButton.setClickable(false);
                }
                checkConvertabe();
            }
        });
    }

    /**
     * Check whether button to convert should be enabled
     * @param
     * @return
     */
    private void checkConvertabe(){
        Button buttonConvert = ((Button) findViewById(R.id.buttonConvert));
        if (!audio && !video || fileList.size() == 0) buttonConvert.setEnabled(false);
        else buttonConvert.setEnabled(true);
    }


}

