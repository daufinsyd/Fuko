package com.example.z003sdxe.myapplication;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayConfAudioActivity extends AppCompatActivity {
    Spinner spinnerAudioCodec;
    EditText editTextQuality;
    EditText editTextBitrate;
    TextView textViewQuality;
    CheckBox checkBoxBitrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_conf_audio);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Get Value passed
        Intent intent = getIntent();
        Boolean audioActivarted = intent.getExtras().getBoolean("com.example.z003sdxe.myapplication.AUDIO_BOOLEAN");

        // Define objects
        spinnerAudioCodec = ((Spinner) findViewById(R.id.spinnerAudioCodec));
        editTextQuality = ((EditText) findViewById(R.id.editTextQuality));
        editTextQuality.setFilters(new InputFilter[]{new EditTextNumberFilter(this, 0,0)});
        editTextQuality.setText(MainActivity.audioQuality.toString());
        textViewQuality = ((TextView) findViewById(R.id.textViewQuality));

        checkBoxBitrate = ((CheckBox) findViewById(R.id.checkBoxBitrate));
        checkBoxBitrate.setChecked(MainActivity.audioSetBirate);
        editTextBitrate = ((EditText) findViewById(R.id.editTextBitrate));
        editTextBitrate.setFilters(new InputFilter[]{new EditTextNumberFilter(this, 0, 1000)});
        editTextBitrate.setText(MainActivity.audioBitrate.toString());
        editTextBitrate.setEnabled(MainActivity.audioSetBirate);


        spinnerAudioCodec.setAdapter(new ArrayAdapter<Enums.AudioCodec>(
                this, R.layout.simple_list_item_1, Enums.AudioCodec.values()
        ));
        spinnerAudioCodec.setSelection(MainActivity.audioCodec.getValue());  //Set index

        addCodecListener();
        addBitRateListener();

    }

    @Override
    public void onPause(){
        super.onPause();
        MainActivity.audioQuality = Integer.parseInt(editTextQuality.getText().toString());
        MainActivity.audioBitrate = Integer.parseInt(editTextBitrate.getText().toString());
        MainActivity.audioSetBirate = checkBoxBitrate.isChecked();
    }

    public void updateCodecOptions(Boolean noUpdate){
        editTextQuality.setEnabled(true);
        editTextBitrate.setVisibility(View.VISIBLE);
        checkBoxBitrate.setVisibility(View.VISIBLE);
        findViewById(R.id.textViewKbps).setVisibility(View.VISIBLE);
        ((EditTextNumberFilter)editTextQuality.getFilters()[0]).setMin(MainActivity.audioCodec.getQualityMin());
        ((EditTextNumberFilter)editTextQuality.getFilters()[0]).setMax(MainActivity.audioCodec.getQualityMax());

        if (!noUpdate) editTextQuality.setText(String.format(Locale.FRANCE, "%d", MainActivity.audioCodec.getQualityDefault()));
        else editTextQuality.setText(String.format(Locale.FRANCE, "%d", MainActivity.audioQuality));

        switch (MainActivity.audioCodec.getQualityType()){
            case -1:
                editTextQuality.setEnabled(false);
                break;
            case 0:  // set quality
                textViewQuality.setText("Qualité");
                break;
            case 1:
                textViewQuality.setText("Taux de compression");
                break;
            default:
                break;
        }
        if (MainActivity.audioCodec == Enums.AudioCodec.FLAC) {
            editTextBitrate.setVisibility(View.INVISIBLE);  // no bitrate
            editTextBitrate.setEnabled(false);
            checkBoxBitrate.setVisibility(View.INVISIBLE);
            checkBoxBitrate.setChecked(false);
            findViewById(R.id.textViewKbps).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                // if not > then restore activity doesn't work; don't know why
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addCodecListener(){
        spinnerAudioCodec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (MainActivity.audioCodec != ((Spinner) findViewById(R.id.spinnerAudioCodec)).getSelectedItem()){
                    MainActivity.audioCodec = ((Enums.AudioCodec)((Spinner) findViewById(R.id.spinnerAudioCodec)).getSelectedItem());
                    updateCodecOptions(false);
                }
                else updateCodecOptions(true);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addBitRateListener(){
        checkBoxBitrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxBitrate.isChecked()) editTextBitrate.setEnabled(true);
                else editTextBitrate.setEnabled(false);
            }
        });
    }
}
