package com.example.z003sdxe.myapplication;

import android.app.DialogFragment;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TabHost;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayAppInfo extends DialogFragment{
    private View view;
    static DisplayAppInfo newInstance() {
        DisplayAppInfo f = new DisplayAppInfo();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        //args.putString("title", title);
        //args.putString("message", message);
        //f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_display_info, container, false);
        initTabHos(this, view);

        return view;
    }

    private void initTabHos(DisplayAppInfo context, View view){
        TabHost tabHost = (TabHost) view.findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tab_app");
        tabSpec.setIndicator("Fuko", getResources().getDrawable(R.drawable.ic_videocam_black_18dp));
        tabSpec.setContent(R.id.tab1);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tab_licence");
        tabSpec.setIndicator("Licence", getResources().getDrawable(R.drawable.ic_videocam_black_18dp));
        tabSpec.setContent(R.id.tab2);
        tabHost.addTab(tabSpec);

    }
}
