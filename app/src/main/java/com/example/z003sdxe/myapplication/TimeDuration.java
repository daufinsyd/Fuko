package com.example.z003sdxe.myapplication;

import java.io.Serializable;
import java.util.Locale;

import javax.xml.datatype.Duration;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class TimeDuration implements Serializable {
    private int duration;  // milliseconds

    TimeDuration(){
        this(0);
    }

    TimeDuration(int duration){
        this.duration = duration;
    }

    TimeDuration(int h, int m, int s){
        this(h, m, s, 0);
    }

    TimeDuration(int h, int m, int s, int ms){
        duration = ms + 1000*(s + 60*(m + 60*h));
    }

    void setDuration(int duration){
        this.duration = duration;
    }

    int getHour(){
        return ((duration/1000)/60)/60;
    }
    int getMinutes(){
        return (duration/1000)/60 - 60*getHour();
    }
    int getSeconds(){
        return duration/1000 - 60*(getMinutes() + 60*getHour());
    }
    int getMilliseconds(){
        return duration - 1000*(getSeconds() + 60*(getMinutes() + 60*getHour()));
    }

    /**
     * @return time in milliseconds
     */
    int getDuration(){
        return  duration;
    }

    /**
     * @return time hh:mm:ss
     */
    String getHMS(){
        return String.format(Locale.FRANCE, "%02d:%02d:%02d", getHour(), getMinutes(), getSeconds());
    }

    /**
     * @return time hh:mm:ss.S
     */
    String getHMSprecise(){
        return getHMS() + "." + getMilliseconds() / 100;
    }

    /**
     * @return time hh:mm:ss:SSSS
     */
    public String getFull(){
        return getHMS() + "." + getMilliseconds();
    }
}
