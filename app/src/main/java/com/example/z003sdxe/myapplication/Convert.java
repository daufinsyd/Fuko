package com.example.z003sdxe.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FilenameUtils;

import com.example.z003sdxe.myapplication.Enums.AudioCodec;

//import processing.ffmpeg.videokit.ProcessingListener;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

class Convert {
    private static List<MyFileItem> currentFileList = new ArrayList<MyFileItem>();

    static void convert(final Context context, final File destDir, final Boolean deleteOrig,
                        final Boolean keepSubtitles) {
        File destFile = null;
        currentFileList = MainActivity.fileList;  // because items removed from fileList when done
        MainActivity.sizeFileList = currentFileList.size();
        // LOAD FFMEPG
        final FFmpeg ffmpeg = FFmpeg.getInstance(context);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                }

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            Toast.makeText(context, "FFmpeg ne peut pas fonctionner sur votre terminal", Toast.LENGTH_LONG).show();
        }
        // SEND START CONVERSION
        Intent intent = new Intent(MainActivity.CONVERT_START_MESSAGE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        for (final MyFileItem item : currentFileList) {
            // ffmpeg is available
            while (ffmpeg.isFFmpegCommandRunning()) {
            }

            // Manage file extension
            String fileName = item.getFileName();
            // If video && video.COPY > remove
            // else if audio && audio.COPY > remove
            // else nop
            if ((!MainActivity.video && MainActivity.audio && MainActivity.audioCodec != AudioCodec.COPY)
                    || (MainActivity.video && MainActivity.videoCodec != Enums.VideoCodec.COPY)) {
                fileName = FilenameUtils.removeExtension(fileName);
            }

            try {
                try {
                    final String filePath = FileUtils.getRealPathFromURI_API19(context, item.getUri());  // Path
                    // todo finish command
                    // cmd = x; cmd += " " + b;   << blank space before new add ! not at end

                    String cmd = "-y -i " + filePath;

                    // Quality
                    String audioQuality = null;
                    String audioCodecFfmpeg = null;
                    if (MainActivity.audioCodec != AudioCodec.COPY)
                        audioCodecFfmpeg = String.format(Locale.FRANCE, " -c:a %s", MainActivity.audioCodec.getFfpmegCode());
                        switch (MainActivity.audioCodec.getQualityType()){
                            case 0:
                                audioQuality = String.format(Locale.FRANCE, " -q:a %d", MainActivity.audioQuality);
                                break;
                            case 1:
                                if (MainActivity.audioCodec == AudioCodec.FLAC) audioQuality = String.format(Locale.FRANCE, " -compression_level %d", MainActivity.audioQuality);
                                break;
                            default:
                                break;
                    }

                    // Bitrate
                    if (MainActivity.audio) {
                        if (audioCodecFfmpeg != null) cmd += audioCodecFfmpeg;
                        if (audioQuality != null) cmd += audioQuality;
                        if (MainActivity.audioSetBirate) {
                            cmd += " -b:a " + MainActivity.audioBitrate * 1000;
                        }
                    }

                    // Audio / video
                    if (MainActivity.video) {
                        if (MainActivity.audioCodec != AudioCodec.COPY)
                            cmd += String.format(" -c:v %s", MainActivity.videoCodec.getFfpmegCode());

                        fileName += MainActivity.videoCodec.getDotExtension();  // If video or video-audio > take video format
                        if (MainActivity.videoSetBirate) {
                            cmd += " -b:v " + MainActivity.videoBitrate * 1000;
                        }
                        // VIDEO ONLY
                        if (!MainActivity.audio) cmd += " -an";  // disable audio
                            // VIDEO + AUDIO
                        else {
                        }
                    }
                    // AUDIO ONLY
                    else {
                        fileName += MainActivity.audioCodec.getDotExtension();
                        cmd += " -vn";  // disable video
                    }

                    // Subtitles
                    if (!keepSubtitles) cmd += " -sn";

                    // Custom time
                    if (MainActivity.customTime && currentFileList.size() == 1) {  // recheck that there is only one file
                        // Warning : should use same getMethod as the one displaying the value in DisplayOptions (getHMSprecise)
                        cmd += " -ss " + MainActivity.customStartTime.getHMSprecise() + " -to " + MainActivity.customEndTime.getHMSprecise();
                    }

                    destFile = new File(destDir + "/" + fileName);
                    cmd += " " + destFile.toString();  // warning ! if not y > if question then ffmpeg waits indefinitively for a response
                    String[] command = cmd.split(" ");

                    // FFMPEG EXEC
                    ffmpeg.execute(command, new FFmpegExecuteResponseHandler() {
                        @Override
                        public void onStart() {
                            Toast.makeText(context, "Sarted", Toast.LENGTH_SHORT).show(); // tdo remove
                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {
                            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                            File log = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_MUSIC + "/Fuko/Error_log.txt");

                            try {
                                FileOutputStream stream = new FileOutputStream(log);
                                stream.write(message.getBytes());
                                stream.close();
                            } catch (IOException e) {
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onSuccess(String message) {
                            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                            MainActivity.fileList.remove(item);

                            File log = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_MUSIC + "/Fuko/success.txt");

                            try {
                                FileOutputStream stream = new FileOutputStream(log);
                                stream.write(message.getBytes());
                                stream.close();
                            } catch (IOException e) {
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }

                            if (deleteOrig) {
                                File file = new File(filePath);
                                Boolean deleted = file.delete();
                            }

                        }

                        @Override
                        public void onFinish() {
                            Toast.makeText(context, "Finished", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MainActivity.CONVERTED_MESSAGE);
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        }
                    });
                } catch (FFmpegCommandAlreadyRunningException e) {
                    // Handle if FFmpeg is already running
                    Toast.makeText(context, "Already running", Toast.LENGTH_SHORT).show();

                }
                // SEND END CONVERSION
                // Cannot because ffmpeg is async
                //intent = new Intent(MainActivity.CONVERT_END_MESSAGE);
                //LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }catch (Error e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }

        }
    }
}