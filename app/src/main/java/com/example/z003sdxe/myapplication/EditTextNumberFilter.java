package com.example.z003sdxe.myapplication;

import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.widget.Toast;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

class EditTextNumberFilter implements InputFilter {
    private Context con;
    private int min, max;

    public EditTextNumberFilter(Context con, int min, int max) {
        this.con = con;
        this.min = min;
        this.max = max;
    }

    public EditTextNumberFilter(String min, String max) {
        this.con = con;
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            String replacement = source.subSequence(start, end).toString();
            String newVal = dest.toString().substring(0, dstart) + replacement +dest.toString().substring(dend, dest.toString().length());
            int input = Integer.parseInt(newVal);
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) {
            Toast.makeText(con, "Number not accepted! Must be between " + min + " and " + max, Toast.LENGTH_SHORT).show();
        }
        return "";
    }

    /**
     * @param min
     * @param max
     * @param input number to check
     * @return Whether min <= input <= max
     */
    private boolean isInRange(int min, int max, int input) {
        return max > min ? input >= min && input <= max : input >= max && input <= min;
    }
}
