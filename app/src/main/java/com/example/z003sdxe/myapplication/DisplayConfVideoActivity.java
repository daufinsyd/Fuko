package com.example.z003sdxe.myapplication;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayConfVideoActivity extends AppCompatActivity {
    Spinner spinnerVideoCodec;
    EditText editTextQuality;
    EditText editTextBitrate;
    TextView textViewQuality;
    CheckBox checkBoxBitrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_conf_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Define objects
        spinnerVideoCodec = ((Spinner) findViewById(R.id.spinnerVideoCodec));
        editTextQuality = ((EditText) findViewById(R.id.editTextQuality));
        editTextQuality.setFilters(new InputFilter[]{new EditTextNumberFilter(this, 0,0)});
        editTextQuality.setText(MainActivity.videoQuality.toString());
        textViewQuality = ((TextView) findViewById(R.id.textViewQuality));


        checkBoxBitrate = ((CheckBox) findViewById(R.id.checkBoxBitrate));
        checkBoxBitrate.setChecked(MainActivity.videoSetBirate);
        editTextBitrate = ((EditText) findViewById(R.id.editTextBitrate));
        editTextBitrate.setFilters(new InputFilter[]{new EditTextNumberFilter(this, 0, 1000)});
        editTextBitrate.setText(MainActivity.videoBitrate.toString());
        editTextBitrate.setEnabled(MainActivity.videoSetBirate);


        spinnerVideoCodec.setAdapter(new ArrayAdapter<Enums.VideoCodec>(
                this, R.layout.simple_list_item_1, Enums.VideoCodec.values()
        ));
        spinnerVideoCodec.setSelection(MainActivity.videoCodec.getValue());  //Set index

        addCodecListener();
        addBitRateListener();
    }

    @Override
    public void onPause(){
        super.onPause();
        MainActivity.videoQuality = Integer.parseInt(editTextQuality.getText().toString());
        MainActivity.videoBitrate = Integer.parseInt(editTextBitrate.getText().toString());
        MainActivity.videoSetBirate = checkBoxBitrate.isChecked();
    }

    public void updateCodecOptions(Boolean noUpdate){
        editTextQuality.setEnabled(true);
        ((EditTextNumberFilter)editTextQuality.getFilters()[0]).setMin(MainActivity.videoCodec.getQualityMin());
        ((EditTextNumberFilter)editTextQuality.getFilters()[0]).setMax(MainActivity.videoCodec.getQualityMax());

        if (!noUpdate) editTextQuality.setText(String.format(Locale.FRANCE, "%d", MainActivity.videoCodec.getQualityDefault()));
        else editTextQuality.setText(String.format(Locale.FRANCE, "%d", MainActivity.videoQuality));

        switch (MainActivity.videoCodec.getQualityType()){
            case -1:
                textViewQuality.setText("Qualité");
                editTextQuality.setEnabled(false);
                break;
            case 0:  // set quality
                textViewQuality.setText("Qualité");
                break;
            case 1:
                textViewQuality.setText("Taux de compression");
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                // if not > then restore activity doesn't work; don't know why
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addCodecListener(){
        spinnerVideoCodec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (MainActivity.videoCodec != ((Spinner) findViewById(R.id.spinnerVideoCodec)).getSelectedItem()){
                    MainActivity.videoCodec = ((Enums.VideoCodec)((Spinner) findViewById(R.id.spinnerVideoCodec)).getSelectedItem());
                    updateCodecOptions(false);
                }
                else updateCodecOptions(true);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addBitRateListener(){
        checkBoxBitrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxBitrate.isChecked()) editTextBitrate.setEnabled(true);
                else editTextBitrate.setEnabled(false);
            }
        });
    }
}
