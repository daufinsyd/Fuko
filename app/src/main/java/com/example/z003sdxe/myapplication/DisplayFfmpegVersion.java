package com.example.z003sdxe.myapplication;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

/**
 * Created by Sydney Gems on 29.08.2017.
 * Fuko - Licence GPLv3
 */

public class DisplayFfmpegVersion extends DialogFragment {
    //String title;
    //String message;

    static DisplayFfmpegVersion newInstance() {
        DisplayFfmpegVersion f = new DisplayFfmpegVersion();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        //args.putString("title", title);
        //args.putString("message", message);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //message = getArguments().getString("message");
        //title = getArguments().getString("title");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_display_ffmpeg, container, false);
        ((TextView) view.findViewById(R.id.textViewTitle)).setText("Version de Ffmpeg");
            String info = null;
            FFmpeg ffmpeg = FFmpeg.getInstance(getContext());
            try {
                ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                    @Override
                    public void onStart() {}

                    @Override
                    public void onFailure() {}

                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onFinish() {}
                });
            } catch (FFmpegNotSupportedException e) {
                // Handle if FFmpeg is not supported by device
            }
            try {
                // to execute "ffmpeg -version" command you just need to pass "-version"
                String [] cmd = new String[1];
                cmd[0] = "-version";
                ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {

                    @Override
                    public void onStart() {}

                    @Override
                    public void onProgress(String message) {}

                    @Override
                    public void onFailure(String message) {}

                    @Override
                    public void onSuccess(String message) {
                        ((TextView) view.findViewById(R.id.textViewMessage)).setText(message);
                    }

                    @Override
                    public void onFinish() {}
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
                // Handle if FFmpeg is already running
            }
        return view;
    }

}
